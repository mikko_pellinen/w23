import React from 'react';

import './App.css';
import Reporter from './Reporter/Reporter';
function App() {
  return (
    <div className="App">
      <Reporter name= "Antero Mertaranta: ">'Löikö Mörkö sisään?'</Reporter>
      <img src={require('./Mörkö.png').default} height={200} width={200} />          
      <Reporter name= "Kevin McGran: ">'I know is's a rough time now, but did
      you at least enjoy playing in the tournament?'</Reporter>
    </div>
  );
}

export default App;
